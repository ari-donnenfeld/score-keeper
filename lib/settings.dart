import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'main.dart';


class Settings extends StatefulWidget {
    Settings({Key? key}) : super(key: key);

    @override
        _SettingsState createState() => _SettingsState();
}

const List<int> playerItems = <int>[1, 2, 3, 4, 5];
enum Shape {
   number, spanish, english  
}

class _SettingsState extends State {

    int players = 2;
    Shape shape = Shape.number;
    bool darkMode = false;


    void saveSettings() async {
        debugPrint("Saving...");
        final prefs = await SharedPreferences.getInstance();

        await prefs.setInt('players', players);
        await prefs.setInt('shape', shape.index);
        await prefs.setBool('darkMode', darkMode);
    }

    void getSaved() async {
        final prefs = await SharedPreferences.getInstance();
        setState(() {
            players = prefs.getInt('players')!;
            int? shapeint = prefs.getInt('shape');
            shape = Shape.values[shapeint??1];
            darkMode = prefs.getBool('darkMode')!;
        });
    }

    @override
    void initState() {
        getSaved();
        super.initState();
    }

    @override
    Widget build(BuildContext context) {

        return Scaffold(
            appBar: AppBar(
                title: Text("Settings"),
                centerTitle: true,
            ),
            body: Container(

                margin: EdgeInsets.all(30),
                child: Column(
                    children: [
                        Row(children: [
                            Text("Players:"),
                            Expanded(child: Container()),
                            DropdownButton(
                                value: players,
                                icon: const Icon(Icons.keyboard_arrow_down),
                                
                                items: playerItems.map((int number) {
                                    return DropdownMenuItem(
                                        child: Text(number.toString()),
                                        value: number
                                    );
                                }).toList(),

                                onChanged: (int? newValue) {
                                    setState(() {
                                        players = newValue!;
                                    });
                                    saveSettings();
                                }
                            )
                        ]),
                        Row(children: [
                            Text("Marker Type:"),
                            Expanded(child: Container()),
                            Row(children: [
                                
                                Container(
                                    padding: const EdgeInsets.all(6.0),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(55),
                                        border: Border.all(color: shape == Shape.number ? Colors.grey : Colors.transparent)
                                        ),
                                    child: MaterialButton(
                                        minWidth: 50,
                                        onPressed: () {setState(() {shape = Shape.number;});saveSettings();},
                                        child: Text("5", style: TextStyle(fontSize: 30))
                                    ),      
                                ),
                                Container(
                                    padding: const EdgeInsets.all(1.0),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(55),
                                        border: Border.all(color: shape == Shape.spanish ? Colors.grey : Colors.transparent)
                                        ),
                                    child: IconButton(
                                      icon: SvgPicture.asset('assets/spanish_5.svg', color: darkMode ? Colors.white : Colors.black),
                                      iconSize: 45,
                                      onPressed: () {setState(() {shape = Shape.spanish;});saveSettings();},
                                    ),
                                ),
                                Container(
                                    padding: const EdgeInsets.all(1.0),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(55),
                                        border: Border.all(color: shape == Shape.english ? Colors.grey : Colors.transparent)
                                        ),
                                    child: IconButton(
                                      icon: SvgPicture.asset('assets/english_5.svg', color: darkMode ? Colors.white : Colors.black),
                                      iconSize: 45,
                                      onPressed: () {setState(() {shape = Shape.english;});saveSettings();},
                                    )
                                )

                            ],)
                        ]),
                        Row(children: [
                            Text("Dark Mode:"),
                            Expanded(child: Container()),
                            Switch(value: darkMode, onChanged: (newBool) {
                                setState(() {
                                    debugPrint("Changing...");
                                    darkMode = newBool;
                                    isDarkTheme.add(newBool);
                                });
                                saveSettings();
                            })
                        ]),
                    ],
                ),
            ),
        );
    }

}
