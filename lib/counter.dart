import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'settings.dart';

class Counter extends StatefulWidget {
    const Counter({
            super.key,
            this.color = const Color(0xFFFFE306),
            required this.shape,
            });

    final Shape shape;
    final Color color;

    @override
        State<Counter> createState() => _CounterState();
}

class _CounterState extends State<Counter> {
    int count = 0;

    @override
    Widget build(BuildContext context) {
        // Setup Relative Sizes
        double screenWidth = MediaQuery.of(context).size.width;

        return Stack(
            children: [
                Column(
                    children: [
                        Expanded(
                            child: GestureDetector(
                                onLongPress: () {
                                    setState(() {
                                        count = 0;
                                    });
                                },
                                onTapDown: (details) {
                                    setState(() {
                                        count += 1;
                                    });
                                    },
                                    child: Container(
                                        margin: const EdgeInsets.all(10.0),
                                        color: Colors.transparent,
                                    ),
                            ),
                        ),
                        Expanded(
                            child: GestureDetector(
                                onLongPress: () {
                                    setState(() {
                                        count = 0;
                                    });
                                },
                                onTapDown: (details) {
                                    setState(() {
                                        count -= 1;
                                    });
                                    },
                                    child: Container(
                                        margin: const EdgeInsets.all(10.0),
                                        color: Colors.transparent,
                                    ),
                            ),
                        ),
                    ]
                ),
                Center(child:
                    Container(margin: EdgeInsets.all(20), child: 
                        widget.shape == Shape.number ? Text(count.toString(), style: TextStyle(fontSize: screenWidth * 0.1))
                        : Wrap(
                            spacing: 8.0, // gap between adjacent chips
                            runSpacing: 4.0, // gap between lines
                            children: [
                                for (int i=4; i<count; i+=5)
                                    SizedBox(child: 
                                        widget.shape == Shape.english ?
                                            SvgPicture.asset('assets/english_5.svg', color: widget.color)
                                            : SvgPicture.asset('assets/spanish_5.svg', color: widget.color),
                                        width: screenWidth * 0.07,
                                        height: screenWidth * 0.07,
                                    ),

                                for (int i=count%5; i!=0; i=0)
                                    SizedBox(child: 
                                        widget.shape == Shape.english ?
                                            SvgPicture.asset('assets/english_${i.toString()}.svg', color: widget.color)
                                            : SvgPicture.asset('assets/spanish_${i.toString()}.svg', color: widget.color),
                                        width: screenWidth * 0.07,
                                        height: screenWidth * 0.07,
                                    ),
                            ],
                        )
                    )
                )
            ]
        );
        }
}
