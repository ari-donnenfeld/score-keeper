import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'settings.dart';
import 'counter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';

void main() {
  runApp(const MyApp());
}

StreamController<bool> isDarkTheme = StreamController();

class MyApp extends StatelessWidget {
    static final ValueNotifier<ThemeMode> themeNotifier =
        ValueNotifier(ThemeMode.light);


    const MyApp({super.key});

    // This widget is the root of your application.
    @override
    Widget build(BuildContext context) {
        return StreamBuilder<bool>(
            initialData: true,
            stream: isDarkTheme.stream,
            builder: (context, snapshot) {
                return MaterialApp(
                    title: 'Score Keeper',
                    theme: ThemeData(
                        // This is the theme of your application.
                        //
                        // Try running your application with "flutter run". You'll see the
                        // application has a blue toolbar. Then, without quitting the app, try
                        // changing the primarySwatch below to Colors.green and then invoke
                        // "hot reload" (press "r" in the console where you ran "flutter run",
                        // or simply save your changes to "hot reload" in a Flutter IDE).
                        // Notice that the counter didn't reset back to zero; the application
                        // is not restarted.
                        primarySwatch: Colors.deepPurple,
                    ),
                    darkTheme: ThemeData(
                        brightness: Brightness.dark,
                        scaffoldBackgroundColor: Colors.black,
                    ),
                    // themeMode: ThemeMode.dark,
                    // themeMode: MyApp.themeNotifier.value,
                    themeMode: snapshot.data ?? false ? ThemeMode.dark : ThemeMode.light,
                    home: const MyHomePage(title: 'Flutter Demo Home Page'),
                );
            }
        );
    }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
    int players = 2;
    Shape shape = Shape.number;
    bool darkMode = false;


    void getSaved() async {
        final prefs = await SharedPreferences.getInstance();
        debugPrint("Showing whats saved");
        setState(() {
            players = prefs.getInt('players')??2;
            int? shapeint = prefs.getInt('shape');
            shape = Shape.values[shapeint??1];
            darkMode = prefs.getBool('darkMode')??false;
            debugPrint(players.toString());
            debugPrint(shape.toString());
            debugPrint(darkMode.toString());
            isDarkTheme.add(darkMode);
        });
    }
  

    @override
    void initState() {
        getSaved();
        SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
            statusBarColor: Colors.transparent,
            statusBarBrightness: Brightness.light,
        ));
        super.initState();
    }

    @override
    Widget build(BuildContext context) {
        // This method is rerun every time setState is called, for instance as done
        // by the _incrementCounter method above.
        //
        // The Flutter framework has been optimized to make rerunning build methods
        // fast, so that you can just rebuild anything that needs updating rather
        // than having to individually change instances of widgets.
        return Scaffold(
            body: Stack(
                children:   [
                    Center(
                    child: Row(
                        children: <Widget>[
                            Expanded(child: 
                                // Tally(test: shape),
                                Counter(shape: shape, color: darkMode ? Colors.white : Colors.black),
                            ),
                            for ( int j=1; j<players; j++ ) Expanded(child: Row( children: <Widget>[
                                Container(
                                    // margin: const EdgeInsets.all(10.0),
                                    // color: Colors.amber[600],
                                    width: 0,
                                    margin: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        color: Colors.white,
                                        boxShadow: [
                                        BoxShadow(color: darkMode ? Colors.white : Colors.black, spreadRadius: 2),
                                        ],
                                    ),
                                ),
                                Expanded(child: 
                                    Counter(shape: shape, color: darkMode ? Colors.white : Colors.black),
                                    // Tally(test: shape),
                                ),
                            ])),
                          ],
                        ),
                    ),
                    Positioned(
                        right: -99,
                        bottom: -99,
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(100),
                            child: Container(
                              height: 150,
                              width: 150,
                              child: GridView.count(
                                primary: false,
                                padding: const EdgeInsets.all(0),
                                crossAxisSpacing: 3,
                                mainAxisSpacing: 3,
                                crossAxisCount: 2,
                                children: [
                                  FilledButton(
                                    onPressed: () async {
                                        await Navigator.push(context, MaterialPageRoute(builder: (context) => Settings()),);
                                        getSaved();
                                    },

                                    child: const Icon(Icons.settings),

                                    // child: Text("1", textAlign: TextAlign.right),
                                    style: FilledButton.styleFrom(
                                      backgroundColor: Colors.grey,
                                    ),
                                  ),
                                  ]
                                )
                              )
                            )
                    ),
                ],
            )
        );
    }
}
